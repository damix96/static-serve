var express = require('express');
var bodyParser = require('body-parser');
var path =require('path');
const app = express()
const file = require('./files');

// file.copy(path.join(__dirname, './files.js'),'yahoo.txt');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(express.static('static/'))


app.get('*', (req, res) => {
// res.send('<img src="/photos/user.svg">')
  res.sendFile('index.html', { root: path.join(__dirname, './static') });
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.send(500, { message: err.message })
})

app.listen(8001, () => {
  console.log(`is now up and running on 8000`)
})
