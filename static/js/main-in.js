$(document).ready(function () {
    $('#fullpage').fullpage({
        verticalCentered: true,
        afterRender: function () {

            //playing the video
            $('video').get(0).play();

        }
    });
});
