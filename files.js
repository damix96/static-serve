var fs = require('fs');

function copy(old,newf,repl) {
  var r = (typeof(repl) == 'boolean')? repl : true;
fs.exists(old,function (exist) {
  if( exist && r ){
    fs.createReadStream(old).pipe(fs.createWriteStream(newf));
  }
})
}
module.exports.copy = copy;
